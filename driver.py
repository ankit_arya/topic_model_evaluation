import MineFeatures
import os

'''LOCATION OF GRAPHS'''
#Graph_path=os.getcwd()+ '/bryan_graphs/en-network/en-100k/en-100k.labels'
#GraphEdges_path=os.getcwd()+ '/bryan_graphs/en-network/en-100k/en-40_normed_100k.edges'
Graph_path=os.getcwd()+ '/bryan_graphs/en-network/myGraphs/100k_labels.txt'
GraphEdges_path=os.getcwd()+ '/bryan_graphs/en-network/myGraphs/edges_graph_SSC_all.txt'



f = open(Graph_path,'r')
f1= open(GraphEdges_path,'r')

print "starting Program"
Data=MineFeatures.MineFeatures()
Data.loadGraph(f)
Data.loadEdges(f1)
print "Done building the Graph"



'''LOCATION OF DATA'''

D4=open('Newman-data/nytimes.topics.txt','r')
D5=open('Newman-data/iabooks.topics.txt','r')


'''RESULTS WOULD BE STORED HERE'''
book_path='/Data/iaBooks/graphs/'
news_path='/Data/NYtimes/graphs/'
Data.genFeatures(D5,book_path)
Data.genFeatures(D4,news_path)
