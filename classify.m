
start=1;
uptil=16;
reg_c=20;


%%% Ia Books CV %%%%%%%%%%%
load('iaBooksData.mat');
Features=Features(:,start:uptil);
Fea=scale_Features(Features);
[max_model,books_c,model]=L1LogitCV(Fea,labels,reg_c);

fileID = fopen('results.txt','w');
fprintf(fileID,'CrossValidation Accuracy of Book %.2f%% \n', max_model)




%%% NY Times CV %%%%%%%%%%%
load('nyTimesData.mat');
Features=Features(:,start:uptil);
Fea=scale_Features(Features);
[max_model,nyTimes_c,model]=L1LogitCV(Fea,labels,reg_c);

fprintf(fileID,'CrossValidation Accuracy of NyTimes %.2f%% \n', max_model);




%%%%%%%%% Train Test Combination %%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('iaBooksData.mat');
Ts=load('nyTimesData.mat');

Features=Features(:,start:uptil);
Test_Features=Ts.Features(:,start:uptil);

accuracy_nyTimes=logistRegClassify(books_c,Features, Test_Features,labels,Ts.labels)
accuracy_iaBooks=logistRegClassify(nyTimes_c, Test_Features,Features,Ts.labels,labels)
%{
start=1;
uptil=16;
reg_c=20;

start2=1;
uptil2=16;

Features=[Features(:,start:uptil);Ts.Features(:,start2:uptil2)];
labels=[labels;Ts.labels];
Fea=scale_Features(Features);
[max_model,c,model]=L1LogitCV(Fea,labels,reg_c);
%}
fprintf(fileID,'Test Accuracy training on Books and test on NYTimes %.2f%% \n', accuracy_nyTimes);
fprintf(fileID,'Test Accuracy training on NyTimes and test on books %.2f%% \n', accuracy_iaBooks);
fclose(fileID);
