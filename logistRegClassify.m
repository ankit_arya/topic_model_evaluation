function [acc] = logistRegClassify(c,Features,Test_Features,Train_labels,Test_labels)

%load('iaBooksData_en40.mat');
%Ts=load('nyTimesData_en40.mat')

%scale the Training features between 0 and 1
I=Features;
for i=1:size(Features,2),
	scaledI = (I(:,i)-min(I(:,i))) ./ (max(I(:,i)-min(I(:,i))));
	Features(:,i)=scaledI;
end

%scale the Testing features between 0 and 1
I=Test_Features;
for i=1:size(Test_Features,2),
	scaledI = (I(:,i)-min(I(:,i))) ./ (max(I(:,i)-min(I(:,i))));
	Test_Features(:,i)=scaledI;
end

sprintf('Done Scaling Features')

X=Features(:,1:16);
Y=Train_labels;
ts_data=Test_Features(:,1:16);
ts_labels=Test_labels;
classifier=6;
best_c=0; max_model=0;


	%LIBLINEAR
	%---------------------------------------------------------------------------------------
	options=sprintf('-s %d -c %d -q -B 1 ',classifier,c);   %liblinear  training options
	model = train(Y,sparse(X), options);    %liblinear call
	[predicted_label, accuracy, prob_estimates] = predict(ts_labels, sparse(ts_data), model,'-b 1');
	acc=accuracy(1);
	
	
	
	
