
%Ia-BOOKS
Y=load('Newman-data/iabooks.eval.txt');

avg_rating=sum(Y')/9;
avg_rating(find(avg_rating <1.5))=1;
avg_rating(find(avg_rating >=1.5))= -1;
labels=avg_rating';

Features=load('Data/iaBooks/graphs/Features.txt');
save('iaBooksData.mat','Features','labels');


clear

%NYTIMES

Y=load('Newman-data/nytimes.eval.txt');
avg_rating=sum(Y')/9;
avg_rating(find(avg_rating <1.5))=1;
avg_rating(find(avg_rating >=1.5))= -1;
labels=avg_rating';

Features=load('Data/NYtimes/graphs/Features.txt');
save('nyTimesData.mat','Features','labels');
