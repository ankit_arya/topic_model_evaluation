Lowest and highest average human-scores

1 = most coherent
3 = least coherent

===============================
1.00  15a. steam engine valve cylinder pressure piston boiler air pump pipe 
1.00  35a. cases bladder disease aneurism tumour sac hernia artery ligature pain 
1.00  38a. window nave aisle transept chapel tower arch pointed arche roof 
1.00  50a. loom cloth thread warp weaving machine wool cotton yarn mill 
1.00  53a. god worship religion sacred ancient image temple sun earth symbol 
1.00  57a. cathedral church tower choir chapel window built gothic nave transept 
1.00  1b. building architecture plan churches design architect century erected mosque roof 
1.00  4b. furniture chair table cabinet wood leg mahogany piece oak louis 
1.00  7b. trout fish fly fishing water angler stream rod flies salmon 
1.00  19b. ware porcelain pottery potter ceramic glaze decoration vases manufacture faience 
1.00  22b. silk lace embroidery tapestry gold embroidered thread linen pattern velvet 
1.00  35b. chancel tower church aisle window nave porch font churches bell 
1.00  36b. pain disease cases symptom acute patient diagnosis fever condition diseases 
1.00  37b. saint virgin angel mary holy child mother christ church heaven 
1.00  43b. sentence verb noun adjective grammar speech pronoun meaning phrases phrase 
----
2.89  43a. twenty power shakspeare black people country sea head mountain ten 
2.89  42a. arc sec free according mark laid open pro account fall 
2.89  25a. digitized google vjooq vjooqic head character period nature account ground 
2.89  22a. mark marked cover rest stand plain added cut bear service 
2.89  14a. commenced period subsequently previously succeeded previous shortly commencement portion occupied 
3.00  60b. head friend fellow captain open sure turn human black late 
3.00  41b. miss interest received evening late hall story public presented past 
3.00  40b. want look going deal try bad tell sure feel remember 
3.00  39b. person occasion purpose respect answer short act sort receive rest 
3.00  9b. soon gave returned replied told appeared arrived received return saw 
3.00  59a. httle hke hfe hght able turn power lost bring eye 
3.00  29a. act sense adv person ppr plant sax genus applied dis 
3.00  19a. soon short longer carried rest turned raised filled turn allowed 
3.00  18a. aud lie bad pro hut pre able nature led want 
3.00  10a. entire finally condition position considered result follow highest greatest fact 
===============================
1.00  5a. space earth moon science scientist light nasa mission planet mar 
1.00  20a. health drug patient medical doctor hospital care cancer treatment disease 
1.00  40a. cell human animal scientist research gene researcher brain university science 
1.00  42a. car ford vehicle model auto truck engine sport wheel motor 
1.00  54a. health care insurance patient hospital medical cost medicare coverage doctor 
1.00  2b. stock market investor fund trading investment firm exchange companies share 
1.00  5b. bush campaign party candidate republican mccain political presidential election governor 
1.00  11b. health disease aids virus vaccine infection hiv cases infected asthma 
1.00  26b. police gun officer crime shooting death killed street victim violence 
1.00  31b. clinton president house white hillary senate lady bill political rodham 
1.00  35b. percent stock market price share million investor rate quarter analyst 
1.00  40b. student university college school professor harvard campus graduate program education 
1.00  52b. court law case lawyer judge federal attorney justice legal trial 
1.00  56b. russian chechnya russia chechen rebel war military troop force moscow 
1.11  1a. iran islamic bin laden muslim afghanistan iranian saudi embassy arab 
----
2.78  32a. telegram worth fort visit wide www online dallas ashbrook church 
2.78  19a. budget coxnews editor coxnet constitution photos graphic bureau layout eastern 
2.89  55b. bad doesnt maybe tell let guy mean isnt better ask 
2.89  51b. globe mass john frank david massachusett bear photos following england 
2.89  48b. thought feel doesnt guy asked wanted tell friend doing went 
2.89  30b. friend thought wanted went knew wasnt love asked guy took 
2.89  22b. oct sept nov aug dec july sun lite adv globe 
2.89  18b. brook stone steven hewlett packard edge borge nov buck given 
2.89  45a. johnson jones miller scott robinson george lawrence murphy mason arnold 
2.89  43a. category houston filed thompson hearst following bonfire mean tag appear 
2.89  10a. constitution color review coxnet page art photos available budget book 
3.00  37b. max crowd hand flag sam white young looked black stood 
3.00  4b. art budget bos code exp attn review add client sent 
3.00  24a. dog moment hand face love self eye turn young character 
3.00  22a. king bond berry bill ray rate james treas byrd key 
===============================

