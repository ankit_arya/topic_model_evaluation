load iabooks.eval.txt
score = mean(iabooks_eval,2)
topics = textread('iabooks.topics.txt','%s','delimiter','\n')
fid = fopen('iabooks.sorted.txt','w');
for t=1:length(topics)
  fprintf(fid,'%.2f\t%s\n', score(t), topics{t})
end
fclose(fid);

load nytimes.eval.txt
score = mean(nytimes_eval,2)
topics = textread('nytimes.topics.txt','%s','delimiter','\n')
fid = fopen('nytimes.sorted.txt','w');
for t=1:length(topics)
  fprintf(fid,'%.2f\t%s\n', score(t), topics{t})
end
fclose(fid);

